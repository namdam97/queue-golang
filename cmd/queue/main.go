package main

import (
	"fmt"

	queue_go "gitlab.com/namdam97/queue-golang/pkg/queue"
)

func main() {
	queue := queue_go.Queue{}
	queue.Enqueue("1: Hoàng Phúc International")
	queue.Enqueue("2: Kappa")
	queue.Enqueue("3: Ecko Unltd")
	queue.Enqueue("4: Superga")
	queue.Enqueue("5: Staple")

	queue.Print()
	fmt.Println()

	fmt.Println(queue.Dequeue())
	queue.Print()
	fmt.Println()

	fmt.Printf("Front %v\n", queue.Front())
	fmt.Printf("Back %v\n", queue.Back())
	queue.Print()
	fmt.Println()

	queue.Clear()
	fmt.Println(queue.Length())
	queue.Print()
}
