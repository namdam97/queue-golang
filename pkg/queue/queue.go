package queue

import "fmt"

type Queue struct {
	Data []interface{}
}

func (q *Queue) Enqueue(element interface{}) {
	q.Data = append([]interface{}{element}, q.Data...)
}

func (q *Queue) Dequeue() interface{} {
	length := len(q.Data)

	if length > 0 {
		front := q.Data[length-1]
		q.Data = q.Data[:length-1]

		return front
	}

	return nil
}

func (q *Queue) Front() interface{} {
	length := len(q.Data)

	if length > 0 {
		return q.Data[length-1]
	}

	return nil
}

func (q *Queue) Back() interface{} {
	length := len(q.Data)

	if length > 0 {
		return q.Data[0]
	}

	return nil
}

func (q *Queue) Length() int {
	return len(q.Data)
}

func (q *Queue) Clear() {
	q.Data = []interface{}{}
}

func (q *Queue) Print() {
	fmt.Printf("Queue %+v\n", q.Data)
}
