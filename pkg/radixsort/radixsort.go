package radixsort

import (
	"fmt"

	queue_go "gitlab.com/namdam97/queue-golang/pkg/queue"
)

var RADIX_LENGTH = 10

func PrintRadix(queues []queue_go.Queue) {
	for i := 0; i < RADIX_LENGTH; i++ {
		fmt.Printf("Bin %d: %+v\n", i, queues[i].Data)
	}
}

func Distribute(numbers []int, queues []queue_go.Queue, digit int) {
	length := len(numbers)

	for i := 0; i < length; i++ {
		if digit == 1 {
			queues[numbers[i]%10].Enqueue(numbers[i])
		} else {
			queues[numbers[i]/10].Enqueue(numbers[i])
		}
	}
}

func Collect(numbers []int, queues []queue_go.Queue) {
	i := 0
	for bin := 0; bin < RADIX_LENGTH; bin++ {
		for queues[bin].Length() != 0 {
			numbers[i] = queues[bin].Dequeue().(int)
			i++
		}
	}
}
