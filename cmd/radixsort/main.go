package main

import (
	"fmt"

	queue_go "gitlab.com/namdam97/queue-golang/pkg/queue"
	radixsort_go "gitlab.com/namdam97/queue-golang/pkg/radixsort"
)

func main() {
	numbers := []int{15, 82, 56, 17, 62, 45, 42, 90, 93, 33, 75, 21}

	queues := make([]queue_go.Queue, radixsort_go.RADIX_LENGTH)
	for i := 0; i < radixsort_go.RADIX_LENGTH; i++ {
		queues[i] = queue_go.Queue{}
	}
	fmt.Println("Before radix sort: ")
	fmt.Println(numbers)
	fmt.Println()

	radixsort_go.Distribute(numbers, queues, 1)
	radixsort_go.PrintRadix(queues)
	radixsort_go.Collect(numbers, queues)
	fmt.Println()

	fmt.Println("After radix with digit 1: ")
	fmt.Println(numbers)
	fmt.Println()

	radixsort_go.Distribute(numbers, queues, 10)
	radixsort_go.PrintRadix(queues)
	radixsort_go.Collect(numbers, queues)
	fmt.Println()

	fmt.Println("After radix sort: ")
	fmt.Println(numbers)
}
